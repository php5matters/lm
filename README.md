# README #

### What is this repository for? ###

* This repository is created for LeloMasti Angular2 Development
* 2.0

**Prerequisite: Install Node.js and npm**

If Node.js and npm aren't already on your machine, install them. Our code requires node v5.x.x or higher and npm 3.x.x or higher. To check which version you are using, run node -v and npm -v in a terminal window.

**Install packages**

Using npm from the command line, install the packages listed in package.json with the command:

```

npm install

```

Error messages—in red—might appear during the install, and you might see npm WARN messages. As long as there are no npm ERR! messages at the end, you can assume success.

If the typings folder doesn't show up after running npm install, you'll need to install it manually with the command:


```

npm run typings install

```

**Build and run the application**

Open a terminal window and enter this command:


```
#!

npm start

```