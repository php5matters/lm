import { LmNg2Page } from './app.po';

describe('lm-ng2 App', function() {
  let page: LmNg2Page;

  beforeEach(() => {
    page = new LmNg2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
